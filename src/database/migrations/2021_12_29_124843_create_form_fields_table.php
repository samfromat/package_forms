<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('form_id');
            $table->string('name', 100);
            $table->string('field_value', 500);
            $table->string('placeholder', 200);
            $table->string('field_id', 200);
            $table->string('field_classes', 200);
            $table->string('label', 200);
            $table->string('type', 50);
            $table->string('attributes', 200);
            $table->string('db_mapping', 100);
            $table->string('list_code', 50);
            $table->string('child_dd_id', 100)->nullable();
            $table->integer('sequence');
            $table->integer('parent_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->tinyInteger('is_deleted')->nullable()->default('0');
            $table->tinyInteger('is_completed')->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_fields');
    }
}
