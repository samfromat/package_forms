<form method="get" id="filterForm" action="@if(isset($filterFormData->form_action_route)){{ $filterFormData->form_action_route }}@endif" autocomplete="off" class="filter w-100 p-2 mb-4 filter-form">
    <input type="hidden" name="service" value="@if(isset($_GET['service'])){{ $_GET['service'] }}@endif">
    <div class="row">
        <div class="col-md-12">
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade in active show" id="filter">
                <div class="">
                    @if(!empty($filterFields) && isset($filterFields))
                    <input type="hidden" value="{{$filterFields[0]->form_id}}" id="form_id">
                    <div class="row pt-2">

                        @foreach($filterFields as $fieldsKey => $fieldsVal)
                            @if(!empty($fieldsVal->type))
                                <div class="col-md-3">
                                    @if( $fieldsVal->type != 'radio')
                                        <label for="filter1" class="control-label">{{ $fieldsVal->label }}</label>
                                    @endif
                                        @if( $fieldsVal->type == 'dropdown')
                                            <?php //echo $fieldsVal; ?>
                                        <div class="form-group" style="margin-left: 1px;">
                                            <select class="form-control chosen-select @if($fieldsVal->field_classes != 'single_select'){{'chosen-select'}}@endif  <?php if($fieldsVal->child_dd_id){ echo 'dependingDropDown';} ?> select-{{ $fieldsVal->id }}"
                                                    data-id="{{ $fieldsVal->id }}"
                                                    data-child-dd_id="{{ $fieldsVal->child_dd_id  }}" id="{{ $fieldsVal->field_id }}" name="{{ $fieldsVal->name }}[]"
                                                    @if($fieldsVal->field_classes != 'single_select') {{ 'multiple'}}@endif multiple>
                                                    <option value="">Select</option>
                                                <?php
                                                $listData = App\common\Common::dropDownList($fieldsVal->list_code, $fieldsVal->parent_id);

                                                if($listData){
                                                foreach ($listData as $listKey){
                                                $selected = '';

                                                if(!empty($settings['settings']['filterData']))
                                                {
                                                    $fieldsValName = $fieldsVal->name;
                                                    if(!empty($settings['settings']['filterData']->$fieldsValName) && in_array($listKey['value'], $settings['settings']['filterData']->$fieldsValName))
                                                    {
                                                        $selected = 'selected';
                                                    }
                                                }
                                                if(!empty($_GET[$fieldsVal->name])){
                                                    if(in_array($listKey['value'], $_GET[$fieldsVal->name]))
                                                    {
                                                        $selected = 'selected';
                                                    }
                                                }
                                                ?>
                                                <option value="{{ $listKey['value'] }}" {{ $selected }}>{{ $listKey['label'] }}</option>
                                                <?php }}?>
                                            </select>
                                        </div>
                                    @elseif( $fieldsVal->type == 'single_date')
                                        <div class="form-group">
                                            <input type="date" class="form-control"  name="selected_date" id="selected_date" value="@if(isset($_GET['date']) && !empty($_GET['date'])){{$_GET['date']}}@endif">
                                        </div>
                                    @elseif( $fieldsVal->type == 'date')
                                        <div class="form-group">

                                            <div id="" style="" class="reportrange form-control border-sky-blue">
                                                <i class="fa fa-calendar"></i>&nbsp;
                                                <span></span> <i class="fa fa-caret-down float-right pt-1"></i>
                                            </div>
                                            <input type="hidden" name="filter_from_date" id="filter_from_date" value="@if(!empty($settings['settings']['filterData']->filter_from_date)){{ $settings['settings']['filterData']->filter_from_date }}@else{{ app('request')->input('filter_from_date') }}@endif">
                                            <input type="hidden" name="filter_to_date" id="filter_to_date" value="@if(!empty($settings['settings']['filterData']->filter_to_date)){{ $settings['settings']['filterData']->filter_to_date }}@else{{ app('request')->input('filter_to_date') }}@endif">

                                        </div>
                                    @elseif( $fieldsVal->type == 'month')
                                        <div class="form-group">
                                            <input type="month" class="form-control" id="month_picker" name="month" value="@if(!empty($settings['settings']['filterData']->month)){{ $settings['settings']['filterData']->month }}@else{{ app('request')->input('filter_from_date') }}@endif">
                                        </div>
                                    @elseif( $fieldsVal->type == 'location')
                                        <div class="form-group ">
                                            <input type="text" name="location" id="autocomplete" value="@if(!empty($settings['settings']['filterData']->location)){{ $settings['settings']['filterData']->location }}@else{{ app('request')->input('location') }}@endif" class="autocomplete form-control border-sky-blue">
                                            <input type="text" name="latitude" id="latitude" value="@if(!empty($settings['settings']['filterData']->latitude)){{ $settings['settings']['filterData']->latitude }}@else{{ app('request')->input('latitude') }}@endif" class="form-control d-none">
                                            <input type="tex" name="longitude" id="longitude" value="@if(!empty($settings['settings']['filterData']->longitude)){{ $settings['settings']['filterData']->longitude }}@else{{ app('request')->input('longitude') }}@endif" class="form-control d-none">
                                        </div>
                                    @elseif( $fieldsVal->type == 'radio')
                                        <div class="form-group">
                                            <input type="radio" name="{{ $fieldsVal->name }}" id="{{ $fieldsVal->field_id }}" value="{{ $fieldsVal->field_value }}"> {{ $fieldsVal->label }}
                                        </div>
                                    @else
                                        <div class="form-group">
                                            <?php $fieldName = $fieldsVal->name; ?>
                                            <input class="form-control" placeholder="{{ $fieldsVal->placeholder }}"
                                                value="@if(isset($settings['settings']['filterData']->$fieldName)){{ $settings['settings']['filterData']->$fieldName }}@endif"
                                                type="{{ $fieldsVal->type }}" name="{{ $fieldsVal->name }}" id="{{ $fieldsVal->field_id }}">
                                        </div>
                                    @endif
                                </div>
                            @endif
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
            @if($settings['group_button'] == 1)
                <div role="tabpanel" class="tab-pane fade" id="group">
                    <div class="">
                        @if(!empty($reportAttribute) && isset($reportAttribute))

                        <div class="row pt-2">
                            <div class="col-md-3">
                                <label for="filter1" class="control-label">Level1</label>
                                <div class="form-group">
                                    <select class="form-control border-sky-blue" id="level1" name="level1" >
                                        <option value="">Select</option>
                                        @foreach($reportAttribute as $key => $val)
                                            <?php
                                            $selected = '';
                                            if(isset($settings['settings']['filterData']->level1) && $settings['settings']['filterData']->level1 == $val->attribute_name){
                                                $selected = 'selected';
                                            }
                                            ?>
                                            <option value="{{ $val->attribute_name }}" <?php echo $selected; ?>>{{ $val->attribute_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label for="filter1" class="control-label">Level2</label>
                                <div class="form-group">
                                    <select class="form-control border-sky-blue" id="level2" name="level2">
                                        <option value="">Select</option>
                                        @foreach($reportAttribute as $key => $val)
                                            <?php
                                            $selected = '';
                                            if(isset($settings['settings']['filterData']->level2) && $settings['settings']['filterData']->level2 == $val->attribute_name){
                                                $selected = 'selected';
                                            }
                                            ?>
                                            <option value="{{ $val->attribute_name }}" <?php echo $selected; ?>>{{ $val->attribute_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <label for="filter1" class="control-label">Level3</label>
                                <div class="form-group">
                                    <select class="form-control border-sky-blue" id="level3" name="level3">
                                        <option value="">Select</option>
                                        @foreach($reportAttribute as $key => $val)
                                            <?php
                                            $selected = '';
                                            if(isset($settings['settings']['filterData']->level3) && $settings['settings']['filterData']->level3 == $val->attribute_name){
                                                $selected = 'selected';
                                            }
                                            ?>
                                            <option value="{{ $val->attribute_name }}" <?php echo $selected; ?>>{{ $val->attribute_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            @endif
        </div>

        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <button class="btn-sm btn-outline-sky-blue filter_buttons btn-filter-search-reset" type="button" name="filterSubmit" value="filterSubmit" id="reset">Reset</button>
            {{--<button class="btn-sm btn-sky-blue filter_buttons btn-filter-search-reset mr-2" type="submit" name="filterSubmit" id="filterSubmit"  value="filterSubmit">Search</button>--}}
            <button class="btn-sm btn-sky-blue filter_buttons btn-filter-search-reset mr-2" type="button" name="filterSubmit" id="filterSubmit" value="filterSubmit">Search</button>
        </div>
    </div>
</form>

    {{-- Date picker js/ css--}}
    <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/daterangepicker.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/daterangepicker.css') }}" />

<script>
    /*-- Location --*/
    google.maps.event.addDomListener(window, 'load', initialize);
    initialize();
    function initialize() {
        var options = {
            componentRestrictions: {country: "IN"}
        };
        geocoder = new google.maps.Geocoder();

        var input = document.getElementById('autocomplete');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            $('#latitude').val(place.geometry['location'].lat());
            $('#longitude').val(place.geometry['location'].lng());

        });
    }

    function errorFunction() {
        alert("Geocoder failed");
    }
    $(document).ready(function () {

        var monthControl = document.querySelector('input[type="month"]');
        var date= new Date()
        var month=("0" + (date.getMonth() + 1)).slice(-2)
        var year=date.getFullYear()
        //monthControl.value = year+'-'+month;

        $('.reportrange').on('apply.daterangepicker', function (ev, picker) {
            $('#filter_from_date').val(picker.startDate.format('YYYY-MM-DD'));
            $('#filter_to_date').val(picker.endDate.format('YYYY-MM-DD'));
        });

        $('#filterSubmit').click(function () {
            //$('.loading').show();
            // var formData    = $("#filterForm :input[value!='']").serialize();
            var formData    =    $('#filterForm').find(":input").filter(function () {return $.trim(this.value).length > 0}).serialize();
            var location    = $('#autocomplete').val();
            var latitude    = $('#latitude').val();
            var longitude   = $('#longitude').val();
            var from_date   = $('#filter_from_date').val();
            var to_date     = $('#filter_to_date').val();
            var month       = $('#month_picker').val();
            var single_date = $('#selected_date').val();
            var url         = window.location.pathname;


            var start = moment().subtract(29, 'days');
            var end = moment();

            console.log(formData);

            if(location != '' && location != undefined && latitude != 'undefined' && longitude != 'undefined' && latitude != '' && longitude != '' && from_date != '' && from_date != 'undefined'){
                url = url+'?'+formData+'&location='+location+'&latitude='+latitude+'&longitude='+longitude;
            }else if(from_date != '' && from_date != 'undefined' && from_date != undefined) {
                //url = url+'?'+formData+'&from_date='+from_date+'&to_date='+to_date;
                url = url+'?'+formData;
            }else if(location != '' && location != undefined && latitude != 'undefined' && longitude != 'undefined' && latitude != '' && longitude != ''){
                var url = url+'?'+formData+'&location='+location+'&latitude='+latitude+'&longitude='+longitude;
            }else if(single_date != ''){
                url = url+'?'+formData+'&date='+single_date;
            }else{
                url = url+'?'+formData;
            }

            $.ajax({
                type: 'GET',
                url: url,
                data: { 'filterSubmit' : 'filterSubmit'},
                success: function(ret_data) {
                    $('.accordion-list').html(ret_data);

                    var config = {
                        '.chosen-select': {},
                        '.chosen-select-deselect': { allow_single_deselect: true },
                        '.chosen-select-no-single': { disable_search_threshold: 10 },
                        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                    }

                    for (var selector in config) {
                        $(selector).chosen(config[selector]);
                    }

                    $(".chosen-select").chosen().change(function () {
                        $(this).val()
                    });

                    /*-- Set date after Filter --*/
                    $(function () {
                        try {
                            var start = moment().subtract(29, 'days');
                            var end = moment();

                            function cb(start, end) {

                                $('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

                            }

                            $('.reportrange').daterangepicker({
                                startDate: start,
                                endDate: end,
                                ranges: {
                                    'Today': [moment(), moment()],
                                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                }
                            }, cb);

                            cb(start, end);
                        } catch (err) {
                            for (var prop in err) {
                                //console.log("property: " + prop + " value: [" + err[prop] + "]\n");
                            }
                        }

                        var start = moment().subtract(29, 'days');
                        var end   = moment();

                        var from_date_parts     = $('#filter_from_date').val().split('-');
                        var format_from_date    = moment(from_date_parts[2] + '.' + from_date_parts[1] + '.' + from_date_parts[0], "DD.MM.YYYY").format("MMMM D, YYYY");

                        var to_date_parts   = $('#filter_to_date').val().split('-');
                        var format_to_date  = moment(to_date_parts[2] + '.' + to_date_parts[1] + '.' + to_date_parts[0], "DD.MM.YYYY").format("MMMM D, YYYY");

                        var from_date   = format_from_date != 'Invalid date' ? format_from_date : start.format('MMMM D, YYYY');
                        var to_date     = format_to_date != 'Invalid date' ? format_to_date : end.format('MMMM D, YYYY');

                        $('.reportrange span').html(from_date + ' - ' + to_date);
                    });

                    $('.loading').hide();
                }
            });
        });

        /* Reset all filter */
        $('#reset').click(function () {
            $('select option:selected').removeAttr('selected');
            // added by Shrikant on 07-Sept-2020. Not able to remove selected item after click on reset.
            $('.chosen-select').val('0').trigger('chosen:updated');
            // end of code.
            $('#autocomplete').val('');
            $('#latitude').val('');
            $('#longitude').val('');
            $('#filter_from_date').val('');
            $('#filter_to_date').val('');
            $('.loading').show();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var url         = window.location.pathname;
            var listing_service_type = getUrlParameter('service');

            $.ajax({
                type: 'GET',
                url: url,
                data: { 'filterSubmit' : $(this).val(),'service':listing_service_type},
                success: function(ret_data) {
                    $('.accordion-list').html(ret_data);

                    var config = {
                        '.chosen-select': {},
                        '.chosen-select-deselect': { allow_single_deselect: true },
                        '.chosen-select-no-single': { disable_search_threshold: 10 },
                        '.chosen-select-no-results': { no_results_text: 'Oops, nothing found!' },
                    }

                    for (var selector in config) {
                        $(selector).chosen(config[selector]);
                    }

                    $(".chosen-select").chosen().change(function () {
                        $(this).val()
                    });

                    /*-- Set date after Filter --*/
                    $(function () {
                        try {
                            var start = moment().subtract(29, 'days');
                            var end = moment();

                            function cb(start, end) {

                                $('.reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

                            }

                            $('.reportrange').daterangepicker({
                                startDate: start,
                                endDate: end,
                                ranges: {
                                    'Today': [moment(), moment()],
                                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                }
                            }, cb);

                            cb(start, end);
                        } catch (err) {
                            for (var prop in err) {
                                //console.log("property: " + prop + " value: [" + err[prop] + "]\n");
                            }
                        }

                        var start = moment().subtract(29, 'days');
                        var end   = moment();

                        var from_date_parts     = $('#filter_from_date').val().split('-');
                        var format_from_date    = moment(from_date_parts[2] + '.' + from_date_parts[1] + '.' + from_date_parts[0], "DD.MM.YYYY").format("MMMM D, YYYY");

                        var to_date_parts   = $('#filter_to_date').val().split('-');
                        var format_to_date  = moment(to_date_parts[2] + '.' + to_date_parts[1] + '.' + to_date_parts[0], "DD.MM.YYYY").format("MMMM D, YYYY");

                        var from_date   = format_from_date != 'Invalid date' ? format_from_date : start.format('MMMM D, YYYY');
                        var to_date     = format_to_date != 'Invalid date' ? format_to_date : end.format('MMMM D, YYYY');

                        $('.reportrange span').html(from_date + ' - ' + to_date);
                    });

                    $('.loading').hide();
                }
            });



            location.reload();
        });
    })
</script>
